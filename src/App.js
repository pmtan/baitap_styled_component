import logo from "./logo.svg";
import "./App.css";
import BaiTapToDoList from "./JSS_StyledComponent/BaiTapToDoList/BaiTapToDoList";

function App() {
  return (
    <div className="App">
      <BaiTapToDoList />
    </div>
  );
}

export default App;
