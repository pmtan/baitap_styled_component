import React, { useRef, useState } from "react";
import { Container } from "../Components/Container";
import { ThemeProvider } from "styled-components";
import { ToDoListDarkTheme } from "./Themes/ToDoListDarkTheme";
import { Dropdown } from "../Components/Dropdown";
import { Heading3 } from "../Components/Heading";
import { Input, Label } from "../Components/TextField";
import { Button } from "../Components/Button";
import { Table, Tr, Th, Thead } from "../Components/Table";
import { themesArray } from "../BaiTapToDoList/Themes/ThemesManager";

export default function BaiTapToDoList() {
  const [toDoList, setToDoList] = useState({
    theme: ToDoListDarkTheme,
    taskList: [
      { id: "task-1", taskName: "Task 1", done: true },
      { id: "task-2", taskName: "Task 2", done: false },
      { id: "task-3", taskName: "Task 3", done: true },
      { id: "task-4", taskName: "Task 4", done: false },
    ],
  });
  const [newTask, setNewTask] = useState({});
  const ref = useRef({ id: "task-2", taskName: "Task 2", done: false });
  const renderTheme = () => {
    return themesArray.map((theme, index) => {
      return (
        <option value={theme.id} key={index}>
          {theme.name}
        </option>
      );
    });
  };
  const renderToDoList = () => {
    let toDo = toDoList.taskList.filter((task) => !task.done);
    return toDo.map((item, index) => {
      return (
        <Tr key={index}>
          <Th>{item.id}</Th>
          <Th className="text-right">
            <Button onClick={() => editTask(item)}>Sua</Button>
            <Button onClick={() => markCompleted(item)}>Xong</Button>
            <Button onClick={() => removeTask(item)}>Xoa</Button>
          </Th>
        </Tr>
      );
    });
  };

  const renderCompletedList = () => {
    let completed = toDoList.taskList.filter((task) => task.done);
    return completed.map((item, index) => {
      return (
        <Tr key={index}>
          <Th>{item.id}</Th>
          <Th className="text-right">
            <Button onClick={() => removeTask(item)}>Xoa</Button>
          </Th>
        </Tr>
      );
    });
  };
  const changeTheme = (e) => {
    let index = themesArray.findIndex((item) => item.id == e.target.value);
    setToDoList({
      ...toDoList,
      theme: themesArray[index].theme,
    });
  };

  const getInput = (e) => {
    let newTask = {
      id: e.target.value,
      [e.target.name]: e.target.value,
      done: false,
    };
    setNewTask(newTask);
  };

  const addTask = (task) => {
    let index = toDoList.taskList.findIndex((item) => item.id === task.id);
    if (index !== -1) {
      alert("Task already exist!");
      return;
    } else {
      let newTaskList = [...toDoList.taskList];
      newTaskList.push(task);
      setToDoList({
        ...toDoList,
        taskList: newTaskList,
      });
      setNewTask({});
      ref.current.value = "";
    }
  };
  const markCompleted = (task) => {
    let updateTaskList = [...toDoList.taskList];
    let index = updateTaskList.findIndex((item) => item.id === task.id);
    updateTaskList[index].done = true;
    setToDoList({
      ...toDoList,
      taskList: updateTaskList,
    });
  };

  const removeTask = (task) => {
    let updateTaskList = [...toDoList.taskList];
    let index = updateTaskList.findIndex((item) => item.id === task.id);
    updateTaskList.splice(index, 1);
    setToDoList({
      ...toDoList,
      taskList: updateTaskList,
    });
  };
  const editTask = (task) => {
    ref.current.value = task.taskName;
    removeTask(task);
  };

  const updateTask = () => {
    // console.log(ref.current.value);
    let newTask = {
      id: ref.current.value,
      taskName: ref.current.value,
      done: false,
    };
    addTask(newTask);
    ref.current.value = "";
  };
  return (
    <div>
      <ThemeProvider theme={toDoList.theme}>
        <Container className="w-50">
          <Dropdown onChange={changeTheme}>{renderTheme()}</Dropdown>
          <Heading3 className="text-left">TO DO LIST</Heading3>
          {/* <TextField
            onChange={getInput}
            name="taskName"
            label="Task Name"
            className="w-50"
          ></TextField> */}
          <span>
            <Label>Task Name</Label>
            <Input ref={ref} onChange={getInput} name="taskName"></Input>
          </span>
          <Button onClick={() => addTask(newTask)} className="ml-3">
            Add Task
          </Button>
          <Button onClick={updateTask} className="ml-3">
            {" "}
            Update Task
          </Button>
          <hr />
          <Heading3>To Do:</Heading3>
          <Table>
            <Thead>{renderToDoList()}</Thead>
          </Table>
          <Heading3>Completed:</Heading3>
          <Table>
            <Thead>{renderCompletedList()}</Thead>
          </Table>
        </Container>
      </ThemeProvider>
    </div>
  );
}
